from flask import Flask, render_template, request, jsonify, Blueprint
from pprint import pprint
from collections import OrderedDict
import mimetypes, json
import engine
import operator
import re
from flask_paginate import Pagination, get_page_parameter, get_page_args

mimetypes.add_type("text/css", ".css", True)

app = Flask(__name__)
data = json.load(open('../corpus/full_format_recipes.json'))
query = ''

def getData(title):
	for x in range(0,len(data)):
		if data[x]:
			if title == data[x]['title']:
				return data[x]

@app.route('/')
def index():
	return render_template('home.html')

@app.route('/search', methods=['POST'])
def search():
	query = [i.lower() for i in re.sub('[^a-zA-Z0-9 ]','', request.form['query']).split()]
	query_soundex = []
	result = {}

	for token in query:
		query_soundex.append(soundex(token))

	for x in range(0,len(data)):
		if data[x]:
			score = 0
			doc = [i.lower() for i in re.sub('[^a-zA-Z0-9 ]','', data[x]['title']).split()]
			doc_soundex = []

			for token in doc:
				doc_soundex.append(soundex(token))

			for y in range(0,len(query)):
				for z in range(0,len(doc)):
					similarity = jaccard_distance(query[y].lower(), doc[z].lower())
					similar = (query_soundex[y].lower() == doc_soundex[z].lower()) and (similarity > 0.2)

					if query[y].lower() == doc[z].lower() or similar:
						if data[x]['title'] in result:
							result[data[x]['title']] = result[data[x]['title']] + (1-(abs(z-y)/len(doc)))
						else:
							result[data[x]['title']] = (1-(abs(z-y)/len(doc)))
			
			if data[x]['title'] in result:
				result[data[x]['title']] = result[data[x]['title']] - (abs(len(doc)-len(query))/len(doc)+len(query))

	result = sorted(result.items(), key=operator.itemgetter(1), reverse=True)

	return render_template('result.html', result=result, query=request.form['query'])


@app.route('/recipe', methods=['GET'])
def recipe():
	food_title = request.args.get('food_name')
	res = getData(food_title)
	
	return render_template('recipe.html', result=res)


@app.route('/similar', methods=['GET'])
def similar():
	result = {}
	regex = '[0-9/,-]|(tea|table)spoon(s?)|stick(s?)|cup(s?)|pound(s?)|ounce(s?)|oz|\(.*\)'
	q_title = request.args.get('food_name')
	q_title_token = [i.lower() for i in re.sub(r'[^a-zA-Z0-9 ]|\b(and|in|with|the|of)\b','', q_title).split()]

	q_food = getData(q_title)
	q_ing = q_food['ingredients']
	q_ing_normalize = [re.sub(regex,'', i).lower() for i in q_ing]

	for x in range(0,len(data)):
		if data[x]:
			score = 0
			for ing in data[x]['ingredients']:
				ing = re.sub(regex,'', ing).lower()
				
				# bobot berdasarkan bahan yang sama
				if ing in q_ing_normalize:
					score += 1

				# bobot berdasarkan judul resep yang ada di bahan
				for token in q_title_token:
					if token in ing:
						score += 3

				# bobot berdasarkan query user yang ada di bahan
				for token in query:
					if token in ing:
						score += 5

			result[data[x]['title']] = score

	result = sorted(result.items(), key=operator.itemgetter(1), reverse=True)

	return render_template('similar.html', result=result, query=q_title)



def soundex(token):
	token = re.sub(r'\B[aiueoyhw]','0', token)
	token = re.sub(r'\B[bfpv]','1', token)
	token = re.sub(r'\B[cgjkqsxz]','2', token)
	token = re.sub(r'\B[dt]','3', token)
	token = re.sub(r'\B[l]','4', token)
	token = re.sub(r'\B[mn]','5', token)
	token = re.sub(r'\B[r]','6', token)

	# remove repeated adjacent character
	token = re.sub(r'[^\w\s]|(.)(?=\1)','', token)

	# remove all 0
	token = re.sub(r'0','', token)

	# four digit coding
	if len(token) < 5:
		for x in range(0, 4-len(token)):
			token += '0'

	if len(token) > 4:
		token = token[0:4]

	token = token[0].upper() + token[1:]

	return token


def jaccard_distance(str1, str2):
	str1 = set(overlap_split(str1,2))
	str2 = set(overlap_split(str2,2))

	return float(len(str1 & str2)) / len(str1 | str2)


def overlap_split(str,n):
	result = []
	
	if(len(str) > n):
		for x in range(0,len(str)-n+1):
			result.append(str[x:x+n])

	return result


if __name__ == '__main__':
	app.run(debug=True)
